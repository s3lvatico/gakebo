package org.gmnz.gakebo.domain;

import java.math.BigDecimal;
import java.util.Date;

public class DiarioSpesa {

   private Date data;
   private Voce voce;
   private BigDecimal importo;

   public Date getData() {
      return data;
   }

   public void setData(Date data) {
      this.data = data;
   }

   public Voce getVoce() {
      return voce;
   }

   public void setVoce(Voce voce) {
      this.voce = voce;
   }

   public BigDecimal getImporto() {
      return importo;
   }

   public void setImporto(BigDecimal importo) {
      this.importo = importo;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((data == null) ? 0 : data.hashCode());
      result = prime * result + ((importo == null) ? 0 : importo.hashCode());
      result = prime * result + ((voce == null) ? 0 : voce.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      DiarioSpesa other = (DiarioSpesa) obj;
      if (data == null) {
         if (other.data != null)
            return false;
      } else if (!data.equals(other.data))
         return false;
      if (importo == null) {
         if (other.importo != null)
            return false;
      } else if (!importo.equals(other.importo))
         return false;
      if (voce == null) {
         if (other.voce != null)
            return false;
      } else if (!voce.equals(other.voce))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "DiarioSpesa [data=" + data + ", importo=" + importo + ", voce=" + voce + "]";
   }

}