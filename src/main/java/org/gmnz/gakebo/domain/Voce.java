package org.gmnz.gakebo.domain;

public class Voce {

   public static enum Segno {
      ENTRATA, USCITA;
   }

   private String id;
   private String nome;
   private boolean abilitato;
   private Categoria categoria;
   private Segno segno;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getNome() {
      return nome;
   }

   public void setNome(String nome) {
      this.nome = nome;
   }

   public boolean isAbilitato() {
      return abilitato;
   }

   public void setAbilitato(boolean abilitato) {
      this.abilitato = abilitato;
   }

   public Categoria getCategoria() {
      return categoria;
   }

   public void setCategoria(Categoria categoria) {
      this.categoria = categoria;
   }

   public Segno getSegno() {
      return segno;
   }

   public void setSegno(Segno segno) {
      this.segno = segno;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      result = prime * result + ((nome == null) ? 0 : nome.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Voce other = (Voce) obj;
      if (id == null) {
         if (other.id != null)
            return false;
      } else if (!id.equals(other.id))
         return false;
      if (nome == null) {
         if (other.nome != null)
            return false;
      } else if (!nome.equals(other.nome))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "Voce [abilitato=" + abilitato + ", categoria=" + categoria + ", id=" + id + ", nome=" + nome + ", segno="
            + segno + "]";
   }

}