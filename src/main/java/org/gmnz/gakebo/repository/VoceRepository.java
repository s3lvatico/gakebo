package org.gmnz.gakebo.repository;

import java.util.List;

import org.gmnz.gakebo.domain.Voce;
import org.gmnz.gakebo.domain.Voce.Segno;



/**
 * CategoriaRepository
 */
public interface VoceRepository {

    List<Voce> getAll();

    void createVoce(Voce v);

    Voce getById(String id);

    Voce getByName(String name);

    List<Voce> getBySegno(Segno segno);

    void update(Voce v);

}