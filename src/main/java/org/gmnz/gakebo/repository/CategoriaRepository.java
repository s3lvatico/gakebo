package org.gmnz.gakebo.repository;

import java.util.List;

import org.gmnz.gakebo.domain.Categoria;

/**
 * CategoriaRepository
 */
public interface CategoriaRepository {

    List<Categoria> getAll();

    void createCategoria(Categoria c);

    Categoria getById(String id);

    Categoria getByName(String name);

    void update(Categoria c);

}