package org.gmnz.gakebo.service;


import java.util.List;
import java.util.UUID;

import org.gmnz.gakebo.domain.Categoria;
import org.gmnz.gakebo.repository.CategoriaRepository;


public class CategoriaService {

	private CategoriaRepository repoCategoria;



	public List<Categoria> getAll() {
		List<Categoria> categoriaList = repoCategoria.getAll();
		return categoriaList;
	}



	public void createCategoria(String name) {
		Categoria c = new Categoria();
		c.setNome(name);
		c.setAbilitato(true);
		c.setId(UUID.randomUUID().toString());
		repoCategoria.createCategoria(c);
	}



	public void rename(String id, String name) {
		Categoria c = repoCategoria.getById(id);
		c.setNome(name);
		repoCategoria.update(c);
	}



	public void disable(String id) {
		Categoria c = repoCategoria.getById(id);
		c.setAbilitato(false);
		repoCategoria.update(c);
	}



	public void enable(String id) {
		Categoria c = repoCategoria.getById(id);
		c.setAbilitato(true);
		repoCategoria.update(c);
	}

}