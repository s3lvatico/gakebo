# I punti da chiarire

1. disegna un diagramma grezzo delle relazioni tra i BOs
2. definisci chiaramente le conseguenze dell'eventuale eliminazione di un'istanza di una voce o di una categoria.


## Relazioni tra i BO

E' un costrutto sufficientemente semplice da poterlo immaginare.

Il BO che sicuramente **non** ha dipendenze è la categoria.
Un altro BO privo di dipendenze è apparentemente anche la voce.

Ma se ci pensi bene, a livello di dipendenze in senso stretto (ossia nel senso che un oggetto usa metodi di un altro oggetto) tutti i BO sono indipendenti.

Categoria : al massimo può mantenere un elenco di Voci

Voce : mantiene un riferimento a una unica Categoria, ed eventualmente a un elenco di DiarioSpesa

DiarioSpesa: può mantenere un riferimento a una Voce

## Nel database

Va stabilito cosa succede quando si disabilita una certa categoria o una voce.

Gli effetti principali si dovrebbero vedere a livello di report

