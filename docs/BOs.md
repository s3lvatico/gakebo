# Oggetti business

## Categoria

La categoria è un insieme semanticamente coerente di voci di spesa - solo e quelle che hanno segno di uscita.

E' generalmente identificata con un nome.

Ha una flag di abilitazione

## Voce

Modella una voce di costo o di ricavo. 

Ha un nome, una flag di abilitazione, un segno che indica se la voce è di costo o di ricavo, ed una categoria associata.

Le voci di ricavo generalmente non dovrebbero avere una categoria.