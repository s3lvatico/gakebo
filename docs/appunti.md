# Appunti su business logic

## Categoria

Classe `CategoriaService`

Operazioni:

- getAll
- crea
- rinomina
- disabilita
- abilita

## Voce

Classe `VoceService`

Operazioni:

- getAll
- trova per categoria (categoria_id)
- trova per segno (entrata/uscita)
- crea (nome, segno, categoria)
- rinomina
- abilita
- disabilita

### getAll()
In quale situazione mi serve avere l'elenco di tutte le voci? Solo quando voglio avere un prospetto globale delle voci e decidere se eseguire operazioni puntuali (su singola voce) o generali (su più voci)

