# UX

## Struttura logica

Do una suddivisione logica, senza idee grafiche, soltanto come raggruppamento delle informazioni e degli elementi dell'interfaccia.

```
.
+-- mese
    +-- generale
    |   +-- preventivo
    |   |   +-- entrate 
    |   |   |   +-- previste
    |   |   |   '-- estemporanee
    |   |   +-- uscite previste
    |   |   '-- disponibilità (semplice o con risparmio)
    |   '-- consuntivo
    |       +-- avanzo primario
    |       +-- totale spese per settimana
    |       '-- totale spese per categoria in ogni settimana
    '-- diario movimenti

```
