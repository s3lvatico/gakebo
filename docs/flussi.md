# Flussi operativi

## Inizio

**Il db è vuoto**. Non ci sono né categorie, né voci di movimento né movimenti.

**Qual è la prima cosa da fare?** Aggiungere categorie di voci.

## Categoria

### [AC1] Visualizzazione categorie presenti

Suddivisione tra categorie abilitate e disabilitate.

~~Per semplicità, due tabelle distinte: una per le categorie abilitate e una per quelle disabilitate. Ogni riga di ciascuna tabella riporta come minimo il nome della categoria.~~ *Tutte cose che riguardano come dev'essere visualizzata l'interfaccia. Non appartengono a questo tipo di documento*

### Creazione nuova

A partire da [AC1]

1. comando per nuova categoria
2. immissione nome
3. conferma / annullamento
4. ritorno a [AC1]

### Modifica esistente

A partire da [AC1]

1. comando per modifica
2. modifica attributi (nome, abilitazione)
3. conferma / annullamento
4. ritorno a [AC1]

## Voce

### [AV1] Visualizzazione voci presenti

Suddivisione tra voci abilitate e disabilitate.

Le voci sono raggruppate per categoria. Le voci con segno di entrata, generalmente, non hanno una categoria.


### Creazione nuova

A partire da [AV1]

1. comando per nuova voce
2. immissione nome, segno e categoria
3. conferma / annullamento
4. ritorno a [AV1]

### Modifica esistente

A partire da [AV1]

1. comando per modifica
2. modifica attributi (nome, abilitazione, categoria, segno)
3. conferma / annullamento
4. ritorno a [AV1]
