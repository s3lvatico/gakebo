# Le cose che si possono fare

## Input

- inserire una previsione di entrata fissa
- inserire un'entrata estemporanea
- inserire una uscita fissa (voce + importo)
- inserire un elemento di diario spesa (data, voce, importo)

## Output

Per fissare le idee, tengo presente l'organizzazione del libro originale. Risulta quindi:

### Entrate

- Prospetto mensile entrate previste
- Prospetto mensile entrate estemporanee
- Prospetto mensile entrate (previste ed estemporanee)

### Uscite

- Prospetto mensile uscite fisse

### Riassunto

- Disponibilità mensile
- Disponibilità mensile con risparmio preimpostato

### Diario mensile 

Il mese è suddiviso in settimane (numerate). Ogni settimana è suddivisa in giorni, all'interno dei quali si gestiscono gli elementi del diario di spesa. I movimenti dovrebbero essere raggruppati per categoria. In ogni settimana dovrebbe essere indicato anche il totale per voce, per categoria e il totale generale delle spese settimanali.

### Consuntivo

**Prospetto delle spese settimanali** : Raccolta dei totali di spesa settimanali per il mese considerato.

**Spaccato delle spese mensili per categoria** : Totale delle spese mensili in ogni categoria, suddivise per settimana

**Risultato in risparmio** : Differenza tra la disponibilità a inizio mese e il totale delle spese mensili.